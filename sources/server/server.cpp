#include <zmq.hpp>
#include <iostream>
#include <unordered_set>

#include <SFML/Graphics.hpp>

int main()
{
    zmq::context_t context(1);
    zmq::socket_t socket(context, zmq::socket_type::router);
    socket.bind("tcp://*:5555");

    std::unordered_set<std::string> connectedClients;

    while (true)
    {
        zmq::message_t identity;
        zmq::message_t delimiter;
        zmq::message_t message;

        zmq::message_t copied_id;
        zmq::message_t copied_msg;

        socket.recv(&identity);
        socket.recv(&delimiter);
        socket.recv(&message);

        if (message.size() > 0 && identity.size() > 0)
        {
            std::string clientId = identity.to_string();

            connectedClients.insert(identity.to_string());

            // Broadcast message to all other clients
            for (const auto &client : connectedClients)
            {
                if (client != identity.to_string())
                {
                    zmq::message_t idmsg(client);
                    socket.send(idmsg, zmq::send_flags::sndmore);

                    socket.send(zmq::message_t{}, zmq::send_flags::sndmore);

                    copied_msg.copy(message);
                    socket.send(copied_msg, zmq::send_flags::none);
                }
            }
        }
    }

    return 0;
}
