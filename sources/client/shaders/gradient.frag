uniform vec2 center;
uniform float radius;

void main() {
    vec2 position = gl_FragCoord.xy - center;
    float distance = length(position);

    if (distance < radius) {
        float gradient = 1.0 - distance / radius;
        gl_FragColor = vec4(1.0, gradient, 0.0, gradient);
    } else {
        discard;
    }

    gl_FragColor = vec4(1.0);
}