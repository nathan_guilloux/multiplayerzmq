#include <iostream>

#include <zmq.hpp>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "player.hpp"
#include "enemy.hpp"
#include "networking.hpp"

int main(int argc, char const *argv[])
{
    const std::vector<int> framerates{15, 30, 60, 144};

    std::srand(std::time(nullptr));
    uint32_t clientID = std::rand();

    zmq::context_t context{1};
    zmq::socket_t socket(context, zmq::socket_type::dealer);
    socket.set(zmq::sockopt::routing_id, std::to_string(clientID));
    socket.connect("tcp://localhost:5555"); // Connect to server

    std::cout << "ClientID: " << clientID << std::endl;

    sf::RenderWindow window(sf::VideoMode(sf::Vector2u(800, 600)), "Game");

    sf::Texture textureAxis;
    if (!textureAxis.loadFromFile("resources/squareaxis.png"))
    {
        std::cerr << "File couldn't load." << std::endl;
    }
    textureAxis.setSmooth(true);

    sf::Sprite spriteDebug{textureAxis};

    sf::Vector2 sizeWindow{window.getSize()};
    Player player{spriteDebug, sf::Vector2{(sizeWindow.x) / 2.0f, (sizeWindow.y) / 2.0f}};

    int windowFramerateLimit = framerates.size() - 1;
    window.setFramerateLimit(framerates[windowFramerateLimit]);

    std::unordered_map<std::string, Enemy *> enemies;

    sf::Clock clock;
    while (window.isOpen())
    {
        // zmq::message_t identity;
        zmq::message_t delimiter;
        zmq::message_t message;
        // socket.recv(&identity, ZMQ_DONTWAIT);
        socket.recv(&delimiter, ZMQ_DONTWAIT);
        socket.recv(&message, ZMQ_DONTWAIT);


        /**
         *     TypeMessage | Message
         *     TypeMessage -> MOVE, ATTACK
         *     Message -> MOVE -> position|rotation
         *                ATTACK -> nothing
         * 
        */

       enum TypeMessage {
            MOVE = 1,
            ATTACK = 2,
       };

       struct Message {
            TypeMessage type;
            std::string id;
            std::string content;
       };

        if (message.size() > 0)
        {
            std::string msg = message.to_string();
            size_t i = msg.find('|');
            std::string id = msg.substr(0, i);
            std::string content = msg.substr(i + 1, msg.size() - (i + 1));

            if (enemies.find(id) == enemies.end())
            {
                Enemy *enemy = new Enemy(spriteDebug);
                enemies[id] = enemy;
            }

            enemies[id]->SerializeFromString(content);
        }

        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::Resized)
            {

                sizeWindow = window.getSize();
                std::cout << sizeWindow.x << ", " << sizeWindow.y << std::endl;
            }
            else if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Key::Up)
                {
                    windowFramerateLimit = (windowFramerateLimit + 1) % framerates.size();
                    window.setFramerateLimit(framerates[windowFramerateLimit]);
                    std::cout << "Change framerate to : " << framerates[windowFramerateLimit] << "HZ\n";
                }
                else if (event.key.code == sf::Keyboard::Key::Down)
                {
                    windowFramerateLimit = (windowFramerateLimit - 1) < 0 ? framerates.size() - 1 : (windowFramerateLimit - 1);
                    window.setFramerateLimit(framerates[windowFramerateLimit]);
                    std::cout << "Change framerate to : " << framerates[windowFramerateLimit] << "HZ\n";
                }
            }

            sf::Vector2f looks = window.mapPixelToCoords(sf::Mouse::getPosition(window));
            player.SetLooks(looks);

            sf::Vector2f inputs;
            inputs.x += sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q) ? -1.0f : 0.0f;
            inputs.x += sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) ? 1.0f : 0.0f;
            inputs.y += sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z) ? -1.0f : 0.0f;
            inputs.y += sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) ? 1.0f : 0.0f;
            player.SetMovements(inputs);
        }

        window.clear(sf::Color::Black);

        sf::Time elapsed = clock.restart();
        float deltaTime = elapsed.asSeconds();

        player.Update(deltaTime);
        player.Draw(window);

        for (const auto &pair : enemies)
        {
            pair.second->Draw(window);
        }

        window.display();

        // send updates to server

        if (player.ShouldUpdateNetwork())
        {
            zmq::message_t delimiter(std::string(""));
            socket.send(delimiter, zmq::send_flags::sndmore);

            std::string serialize{std::to_string(clientID) + "|" + player.SerializeToString()};

            zmq::message_t message(serialize);
            socket.send(message, zmq::send_flags::dontwait);

            player.UpdatedNetwork();
        }
    }

    return EXIT_SUCCESS;
}
