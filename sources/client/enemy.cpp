#include "enemy.hpp"

Enemy::Enemy(sf::Sprite sprite, sf::Vector2f position) : Player{sprite, position}
{
    m_sprite.setColor(sf::Color::Red);
}

Enemy::~Enemy()
{
}

Enemy::Enemy(const Enemy &enemy) : Player{enemy.m_sprite, enemy.getPosition()}
{
}

Enemy &Enemy::operator=(const Enemy &enemy)
{
    SetSprite(enemy.m_sprite);
    setPosition(enemy.getPosition());

    return *this;
}