#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <iostream>
#include <numbers>

#include <SFML/Graphics.hpp>

#include "serialisable.hpp"

class Player : public sf::Transformable, public Serializable
{
protected:
    sf::Sprite m_sprite;
    float m_speed;
    bool m_bHasMoved;

    sf::Vector2f m_looks, m_movements;

public:
    Player(sf::Sprite sprite, sf::Vector2f position = sf::Vector2f{0.0f, 0.0f});
    ~Player();

    void Update(float deltaTime);
    void Draw(sf::RenderTarget &target);
    void Draw(sf::RenderTarget &target) const;

    void HandleMovement(float deltaTime);
    void HandleRotation(float deltaTime);

    virtual bool SerializeFromString(std::string obj) override;
    virtual std::string SerializeToString() const override;

    // Getter / Setter
public:
    void SetLooks(sf::Vector2f inputs) { m_looks = inputs; }
    void SetMovements(sf::Vector2f inputs) { m_movements = inputs; }

    sf::Sprite GetSprite() { return m_sprite; }
    void SetSprite(sf::Sprite sprite) { m_sprite = sprite; }

    sf::Vector2f GetMiddleAnchor() { return m_sprite.getLocalBounds().getCenter(); }
    bool ShouldUpdateNetwork() { return m_bHasMoved; }
    void UpdatedNetwork() { m_bHasMoved = false; }
};

#endif