add_executable(
    "${PROJECT_NAME}_client"
    client.cpp
    player.cpp
    enemy.cpp
    networking.cpp
    serialisable.cpp
)

target_link_libraries("${PROJECT_NAME}_client" PRIVATE sfml-graphics cppzmq)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    file(COPY ${CMAKE_CURRENT_LIST_DIR}/shaders DESTINATION ${CMAKE_BINARY_DIR}/bin/Debug)
    file(COPY ${CMAKE_CURRENT_LIST_DIR}/resources DESTINATION ${CMAKE_BINARY_DIR}/bin/Debug)
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
    file(COPY ${CMAKE_CURRENT_LIST_DIR}/shaders DESTINATION ${CMAKE_BINARY_DIR}/bin/Release)
    file(COPY ${CMAKE_CURRENT_LIST_DIR}/resources DESTINATION ${CMAKE_BINARY_DIR}/bin/Release)
else()
    message("No Build mode information, can't copy resources.")
endif()