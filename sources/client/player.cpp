#include "Player.hpp"

Player::Player(sf::Sprite sprite, sf::Vector2f position) : m_sprite{sprite}, m_speed{250.0f}, m_bHasMoved{false}
{
    setOrigin(GetMiddleAnchor());
    setPosition(position);
}

Player::~Player()
{
}

void Player::Update(float deltaTime)
{
    HandleMovement(deltaTime);
    HandleRotation(deltaTime);
}

void Player::Draw(sf::RenderTarget &target)
{
    target.draw(m_sprite, getTransform());
}

void Player::Draw(sf::RenderTarget &target) const
{
    target.draw(m_sprite, getTransform());
}

void Player::HandleMovement(float deltaTime)
{
    if (m_movements != sf::Vector2<float>(0.0f, 0.0f))
    {
        sf::Vector2 direction = m_movements.normalized();
        move(m_speed * deltaTime * direction);
        m_bHasMoved = true;
    }
}

void Player::HandleRotation(float deltaTime)
{
    sf::Vector2 delta = m_looks - getPosition();
    float rotation = std::atan2(delta.y, delta.x);
    setRotation(sf::radians(rotation));
}

bool Player::SerializeFromString(std::string obj)
{
    if (obj.size() == 0) return false;

    float x = 0.0f;
    float y = 0.0f;
    float rotation = 0.0f;

    int args = 0;
    int left = 0;

    for (int right = 0; right < obj.size(); right++)
    {
        if (obj[right] == '|')
        {
            if (args == 0)
                x = std::stof(obj.substr(left, right - left));
            else if (args == 1)
                y = std::stof(obj.substr(left, right - left));
            left = right + 1;
            args++;
        }
    }

    rotation = std::stof(obj.substr(left, obj.size() - left));

    setPosition(sf::Vector2f{x, y});
    setRotation(sf::radians(rotation));

    return true;
}

std::string Player::SerializeToString() const
{
    std::string serialization("");

    sf::Vector2f position = getPosition();
    serialization.append(std::to_string(position.x) + "|" + std::to_string(position.y));

    float rotation = getRotation().asRadians();
    serialization.append("|" + std::to_string(rotation));

    return serialization;
}