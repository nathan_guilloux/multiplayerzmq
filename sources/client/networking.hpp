#ifndef NETWORKING_HPP
#define NETWORKING_HPP

#include <iostream>

enum ActionType {
    MOVE,
    INTERACT,
    ATTACK
};

enum MessageType
{
    ENTITY_MOVE,
    PLAYER_ACTION
};

struct EntityMove
{
    int entity_id;
    float x_position;
    float y_position;
    float rotation;
};

struct PlayerAction
{
    int player_id;
    ActionType action_type;
    float x_position;
    float y_position;
    float rotation;
};

#endif