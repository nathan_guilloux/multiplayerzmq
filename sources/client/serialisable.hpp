#ifndef SERIALIZABLE_HPP
#define SERIALIZABLE_HPP

#include <iostream>

class Serializable
{
public:
    virtual bool SerializeFromString(std::string obj) = 0;
    virtual std::string SerializeToString() const = 0;
};

#endif