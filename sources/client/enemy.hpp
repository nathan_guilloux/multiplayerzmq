#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <iostream>

#include <SFML/Graphics.hpp>

#include "player.hpp"

class Enemy : public Player
{
private:
    uint32_t m_id;

public:
    Enemy(sf::Sprite sprite, sf::Vector2f position = sf::Vector2f{0.0f, 0.0f});
    ~Enemy();

    // Copy constructor
    Enemy(const Enemy &enemy);
    // Assignment operator
    Enemy &operator=(const Enemy &enemy);

    // Getter / Setter
public:
    void SetID(uint32_t id) { m_id = id; }
    uint32_t GetID() { return m_id; }
};

#endif